<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Ubah Huruf</title>
</head>
<body>
	<h1>Ubah Huruf</h1>

	<?php
		function ubah_huruf($string)
		{
			$huruf = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't','u','v','w','x','y','z'];
			 $tempat = "";

			 for ($i = 0; $i < strlen($string); $i++) {
			 	for ($x = 0; $x < count($huruf); $x++) {
			 		if ($huruf[$x] == $string[$i]) {			 			
			 			$tempat .= $huruf[$x + 1];
			 		} 
				}
			 }
			 echo $string . ' => ' ;
			 echo $tempat . '<br><br>';
		}

		echo ubah_huruf('wow'); // xpx
		echo ubah_huruf('developer'); // efwfmpqfs
		echo ubah_huruf('laravel'); // mbsbwfm
		echo ubah_huruf('keren'); // lfsfo
		echo ubah_huruf('semangat'); // tfnbohbu
	?>
</body>
</html>
